# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2021-09-17
### Added
- Standard `essioc` configuration to example startup script
- Fake dependency on `stream`

### Changed
- Improve fake dependencies in `README.md`
- Fix immutable references used in module configuration
- Updates to versions of EPICS base and *require*
- Fix module configuration (use the common setting for siteMods)

### Removed
- Wrapper config files in local module
- Empty `patch/` directory
- Empty `template/` directory
- Empty `opi/` directory
- Template code in e3 makefile

## [0.1.0] - 2020-12-09
First release


[Unreleased]: https://gitlab.esss.lu.se/e3/e3-examplemodule/-/compare/0.1.0...HEAD
[1.0.0]: https://gitlab.esss.lu.se/e3/e3-examplemodule/-/compare/0.1.0...0.1.0
[0.1.0]: https://gitlab.esss.lu.se/e3/e3-examplemodule/-/tags/0.1.0
# e3-examplemodule

This module performs absolutely no functions on any types of hardware, but rather is intended to guide e3 users and developers on how a wrapper repository should be modeled.

For a project history, see the [CHANGELOG.md](CHANGELOG.md).

## Requirements

- `yaml-cpp`
- `libusb`

## EPICS dependencies

```sh
$ make dep
require examplemodule,1.0.0
< configured ...
COMMON_DEP_VERSION = 1.0.0
> generated ...
common 1.0.0
```

## Installation

```sh
$ make init patch build
$ make install
```

For further targets, type `make`.

## Usage

```sh
$ iocsh.bash -r examplemodule
```

## Additional information

Put design info or links (where the real pages could be in e.g. `docs/design.md`, `docs/usage.md`) to design info here.

## Contributing

Contributions through pull/merge requests only.